import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random
import numpy as np

def quicksort(array, start, end, imgs):
    if start >= end:
        return

    pivot_idx = partition(array, start, end)
    imgs.append(np.copy(array))

    quicksort(array, start, pivot_idx-1, imgs)
    quicksort(array, pivot_idx+1, end, imgs)

def partition(array, start, end):
    pivot = array[start]
    low = start + 1
    high = end

    while True:
        while low <= high and array[high] >= pivot:
            high = high - 1

        while low <= high and array[low] <= pivot:
            low = low + 1

        if low <= high:
            array[low], array[high] = array[high], array[low]
        else:
            break

    array[start], array[high] = array[high], array[start]

    return high

# Générer un tableau de données aléatoires
data = random.sample(range(1, 101), 100)

# Liste pour stocker les états du tableau
imgs = [np.copy(data)]

# Effectuer le tri rapide
quicksort(data, 0, len(data)-1, imgs)

# Créer une figure et des axes
fig, ax = plt.subplots()

# Créer une barre pour chaque élément de la liste
bars = ax.bar(range(len(data)), imgs[0], align='edge')

# Définir les limites de l'axe des ordonnées
ax.set_ylim(0, max(data)+5)

# Fonction pour mettre à jour le graphique
def update_fig(array, bars):
    for bar, val in zip(bars, array):
        bar.set_height(val)
    return bars

# Créer l'animation
ani = animation.FuncAnimation(fig, update_fig, fargs=(bars, ), frames=imgs, repeat=False, blit=True)

plt.show()
