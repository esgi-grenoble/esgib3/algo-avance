import matplotlib.pyplot as plt
import random

# Fonction de tri par sélection avec visualisation
def selection_sort_visualized(arr):
    fig, ax = plt.subplots()
    for i in range(len(arr)):
        min_idx = i
        for j in range(i+1, len(arr)):
            if arr[min_idx] > arr[j]:
                min_idx = j
        
        # Échanger les éléments
        arr[i], arr[min_idx] = arr[min_idx], arr[i]
        
        ax.clear()
        ax.bar(range(len(arr)), arr, color='blue')
        plt.xlabel('Index')
        plt.ylabel('Valeur')
        plt.title('Tri par sélection en action')
        
        # Mettre en évidence l'élément actuellement sélectionné
        ax.bar(i, arr[i], color='red')
        plt.pause(0.5)
    
    plt.show()

# Générer une liste aléatoire de nombres
n = 20
random_list = random.sample(range(1, n + 1), n)

selection_sort_visualized(random_list)
