# Algo-avance



## Introduction
Ce projet a pour but de mettre en pratique les algorithmes avancés vus en cours. Il s'agit de résoudre des problèmes de listes, de tris, d'arbres et de cryptographie.
Ce répertoire contient les notebooks python des différents problèmes ainsi que les fichiers de données nécessaires à leur résolution.

### Base python
Dans ce fichier notebook, nous avons implémenté des fonctions de base en python. Il s'agit de fonctions permettant de manipuler des listes, des dictionnaires, des ensembles, des chaînes de caractères. 


